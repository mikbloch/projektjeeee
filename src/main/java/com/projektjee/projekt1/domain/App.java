package com.projektjee.projekt1.domain;

import com.opencsv.bean.CsvBindByName;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;


@Data
public class App {
    @CsvBindByName(column = "id")
    private int id;
    @NotBlank
    @Pattern(regexp = "[A-Z]([a-z]*[/-]?)*([ -][A-Z][a-z]*)*", message = "Begin words with Capital Letters")
    @Pattern(regexp = "^[A-Za-z -]*$", message = "Valid characters include only letters, spaces and dashes")
    @CsvBindByName(column = "Aplication name")
    private String name;
    @NotBlank
    @Pattern(regexp = "([a-z]+[.])+[a-z]+", message = "use only dots and small letters")
    @CsvBindByName(column = "Domain name")
    private String domain;
}
