package com.projektjee.projekt1.domain;

import com.opencsv.bean.CsvBindByName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;


@Data
@AllArgsConstructor @NoArgsConstructor
public class Person {
    @CsvBindByName(column = "id")
    private int id;
    @NotBlank
    @Pattern(regexp = "[A-Z][a-z]+", message = "Insert Capitalized First Name")
    @CsvBindByName(column = "first_name")
    private String firstName;
    @NotBlank
    @Pattern(regexp = "[A-Z][a-z]*([' ]?[A-Z][a-z])*", message = "Insert Capitalized Last Name")
    @CsvBindByName(column = "last_name")
    private String lastName;
    @NotBlank
    @Pattern(regexp = "[a-z0-9]+[@][a-z0-9]+[.][a-z]+", message = "Insert correct email")
    @CsvBindByName(column = "email")
    private String email;
    @NotBlank
    @Pattern(regexp = "[A-Z][a-z]+", message = "Insert correct Country")
    @CsvBindByName(column = "Country")
    private String country;
    @NotBlank
    @Pattern(regexp = "[a-z0-9]+", message = "use only numvers and small letters")
    @CsvBindByName(column = "Username")
    private String userName;
    @Pattern(regexp = ".{6,}", message = "password needs to be at least 6 signs long")
    @Pattern(regexp = "^[A-Za-z0-9]*$", message = "password contains invalid signs")
    @NotBlank
    @CsvBindByName(column = "Password")
    private String password;

    public Person(@NotBlank @Pattern(regexp = "[A-Z][a-z]+") String firstName, @NotBlank @Pattern(regexp = "[A-Z][a-z]*([' ]?[A-Z][a-z])*") String lastName, @NotBlank @Pattern(regexp = "[a-z0-9]+[@][a-z0-9]+[.][a-z]+") String email, @NotBlank @Pattern(regexp = "[A-Z][a-z]+") String country, @NotBlank @Pattern(regexp = "[a-z0-9]+") String userName, @Pattern(regexp = ".{6,}", message = "password needs to be at least 6 signs long") @Pattern(regexp = "^[A-Za-z0-9]*$", message = "password contains invalid signs") @NotBlank String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.country = country;
        this.userName = userName;
        this.password = password;
    }
}

