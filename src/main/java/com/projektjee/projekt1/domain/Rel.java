package com.projektjee.projekt1.domain;

import com.opencsv.bean.CsvBindByName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Min;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Rel {
    @Min(value = 1)
    @CsvBindByName(column = "personId")
    private int idPerson;
    @Min(value = 1)
    @CsvBindByName(column = "appId")
    private int idApp;
}
