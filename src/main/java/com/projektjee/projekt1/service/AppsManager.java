package com.projektjee.projekt1.service;

import com.projektjee.projekt1.domain.App;
import com.projektjee.projekt1.domain.Person;

import java.util.List;

public interface AppsManager {
    void add(App app);

    App findById(int id);

    List<App> getAllApps();

    void edit(App app);

    void setApps(List<App> apps);

    void remove(int id);
}
