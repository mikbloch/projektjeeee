package com.projektjee.projekt1.service;


import com.projektjee.projekt1.domain.Person;

import java.util.List;

public interface PersonManager {
    void add(Person person);

    Person findById(int id);

    List<Person> getAllPersons();

    void edit(Person person);

    void setPersons(List<Person> persons);

    void remove(int id);

}
