package com.projektjee.projekt1.service;

import com.projektjee.projekt1.domain.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class PersonInMemoryService implements PersonManager {

    @Autowired
    private List<Person> persons;

    @Override
    public void add(Person person) {
        person.setId(persons.get(persons.size() - 1).getId() + 1);
        persons.add(person);
    }

    @Override
    public Person findById(int id) {
        for (Person person : persons) {
            if (person.getId() == id) {
                return person;
            }
        }
        return null;
    }

    @Override
    public List<Person> getAllPersons() {
        return persons;
    }

    @Override
    public void edit(Person person) {
        for (Person toEditPerson : persons) {
            if (person.getId() == toEditPerson.getId()) {
                toEditPerson.setFirstName(person.getFirstName());
                toEditPerson.setLastName(person.getLastName());
                toEditPerson.setEmail(person.getEmail());
                toEditPerson.setCountry(person.getCountry());
                toEditPerson.setUserName(person.getUserName());
                toEditPerson.setPassword(person.getPassword());
                break;
            }
        }
    }

    @Override
    public void setPersons(List<Person> persons) {
        this.persons = persons;
    }

    @Override
    public void remove(int id) {
        for (Person personTodie : persons) {
            if (personTodie.getId() == id) {
                persons.remove(personTodie);
                break;
            }
        }
    }

    public Map<String, Integer> countCountries(List<Person> persons) {
        List<String> countries = new ArrayList<>();
        List<Integer> amount = new ArrayList<>();
        Map<String, Integer> result = new HashMap<>(    );
        for (int i=0;i<persons.size();i++){
            if (countries.contains(persons.get(i).getCountry())){
                int j = countries.indexOf(persons.get(i).getCountry());
                amount.set(j, amount.get(j)+1);
            }
            else {
                countries.add(persons.get(i).getCountry());
                amount.add(1);
            }
        }
        for (int i=0;i<countries.size();i++){
            result.put(countries.get(i), amount.get(i));
        }
        return result;
    }

}
