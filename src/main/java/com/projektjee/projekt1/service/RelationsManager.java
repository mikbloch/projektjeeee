package com.projektjee.projekt1.service;

import com.projektjee.projekt1.domain.App;
import com.projektjee.projekt1.domain.Person;
import com.projektjee.projekt1.domain.Rel;

import java.util.List;

public interface RelationsManager {
    void add(Rel rel);

    Rel findByIds(int appId, int personId);

    List<Person> findPeopleByAppId(int appId);

    List<App> findAppsByPersonId(int personId);

    List<Rel> getAllRelations();

    void remove(Rel rel);

    void removeByPersonId(int personId);

    void removeByAppId(int personId);
}
