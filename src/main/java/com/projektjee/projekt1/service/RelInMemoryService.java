package com.projektjee.projekt1.service;

import com.projektjee.projekt1.domain.App;
import com.projektjee.projekt1.domain.Person;
import com.projektjee.projekt1.domain.Rel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
public class RelInMemoryService implements RelationsManager{

    @Autowired
    private List<Rel> rels;

    @Autowired
    AppInMemoryService apps;
    @Autowired
    PersonInMemoryService ppl;

    @Override
    public void add(Rel rel) {
        rels.add(rel);
    }

    @Override
    public Rel findByIds(int appId, int personId) {
        for (Rel rel : rels) {
            if (rel.getIdApp() == appId && rel.getIdPerson() == personId) {
                return rel;
            }
        }
        return null;
    }

    @Override
    public List<Person> findPeopleByAppId(int appId) {
        List<Person> result = new ArrayList<>();
        for (Rel rel : rels) {
            if (rel.getIdApp() == appId) {
                result.add(ppl.findById(rel.getIdPerson()));
            }
        }
        return result;
    }

    @Override
    public List<App> findAppsByPersonId(int personId) {
        List<App> result = new ArrayList<>();
        for (Rel rel : rels) {
            if (rel.getIdPerson() == personId) {
                result.add(apps.findById(rel.getIdApp()));
            }
        }
        return result;
    }


    @Override
    public List<Rel> getAllRelations() {
        return rels;
    }

    @Override
    public void remove(Rel rel) {
        rels.remove(rel);
    }

    @Override
    public void removeByPersonId(int personId) {
        List<Rel> relsToDie = new ArrayList<>();
        for (Rel rel: rels) {
            if (rel.getIdPerson() == personId) {
                relsToDie.add(rel);
            }
        }
        for (int i=0;i<relsToDie.size();i++){
            rels.remove(relsToDie.get(0));
        }
    }


    @Override
    public void removeByAppId(int appId) {
        List<Rel> relsToDie = new ArrayList<>();
        for (Rel rel: rels) {
            if (rel.getIdApp() == appId) {
                relsToDie.add(rel);
            }
        }
        for (int i=0;i<relsToDie.size();i++){
            rels.remove(relsToDie.get(0));
        }
    }
}
