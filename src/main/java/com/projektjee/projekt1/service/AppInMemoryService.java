package com.projektjee.projekt1.service;

import com.projektjee.projekt1.domain.App;
import com.projektjee.projekt1.domain.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AppInMemoryService implements AppsManager{
    @Autowired
    private List<App> apps;

    @Override
    public void add(App app) {
        app.setId(apps.get(apps.size() - 1).getId() + 1);
        apps.add(app);
    }

    @Override
    public App findById(int id) {
        for (App app : apps) {
            if (app.getId() == id) {
                return app;
            }
        }
        return null;
    }

    @Override
    public List<App> getAllApps() {
        return apps;
    }

    @Override
    public void edit(App app) {
        for (App appToEdit : apps) {
            if (app.getId() == appToEdit.getId()) {
                appToEdit.setDomain(app.getDomain());
                appToEdit.setName(app.getName());
                break;
            }
        }
    }

    @Override
    public void setApps(List<App> apps) {
        this.apps = apps;
    }

    @Override
    public void remove(int id) {
        for (App app : apps) {
            if (app.getId() == id) {
                apps.remove(app);
                break;
            }
        }
    }
}
