package com.projektjee.projekt1.service;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;
import com.projektjee.projekt1.domain.App;
import com.projektjee.projekt1.domain.Person;
import com.projektjee.projekt1.domain.Rel;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.beans.XMLEncoder;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Configuration
public class DomainConfig {
    @Bean
    public List<Person> ppl() {
        List<Person> persons = null;
        String fileName = "src/main/resources/dataPerson.csv";
        Path myPath = Paths.get(fileName);

        try (BufferedReader br = Files.newBufferedReader(myPath,
                StandardCharsets.UTF_8)) {

            HeaderColumnNameMappingStrategy<Person> strategy
                    = new HeaderColumnNameMappingStrategy<>();
            strategy.setType(Person.class);

            CsvToBean csvToBean = new CsvToBeanBuilder(br)
                    .withType(Person.class)
                    .withMappingStrategy(strategy)
                    .withIgnoreLeadingWhiteSpace(true)
                    .build();

            persons = csvToBean.parse();
        }
        catch (Exception e){
            e.printStackTrace();
        }

        try {
            FileOutputStream fos = new FileOutputStream("src/main/resources/personsbeans.xml");
            BufferedOutputStream bos = new BufferedOutputStream(fos);
            XMLEncoder xmlEncoder = new XMLEncoder(bos);
            xmlEncoder.writeObject(persons);
            xmlEncoder.close();
        }
        catch (Exception e){
            e.printStackTrace();
        }

        return new ArrayList<>(persons);
    }
    @Bean
    public List<App> apps() {
        List<App> apps = null;
        String fileName = "src/main/resources/dataApp.csv";
        Path myPath = Paths.get(fileName);

        try (BufferedReader br = Files.newBufferedReader(myPath,
                StandardCharsets.UTF_8)) {

            HeaderColumnNameMappingStrategy<App> strategy
                    = new HeaderColumnNameMappingStrategy<>();
            strategy.setType(App.class);

            CsvToBean csvToBean = new CsvToBeanBuilder(br)
                    .withType(App.class)
                    .withMappingStrategy(strategy)
                    .withIgnoreLeadingWhiteSpace(true)
                    .build();

            apps = csvToBean.parse();
        }
        catch (Exception e){
            e.printStackTrace();
        }

        try {
            FileOutputStream fos = new FileOutputStream("src/main/resources/appsbeans.xml");
            BufferedOutputStream bos = new BufferedOutputStream(fos);
            XMLEncoder xmlEncoder = new XMLEncoder(bos);
            xmlEncoder.writeObject(apps);
            xmlEncoder.close();
        }
        catch (Exception e){
            e.printStackTrace();
        }

        return new ArrayList<>(apps);
    }
    @Bean
    public List<Rel> rels() {
        List<Rel> relations = null;
        String fileName = "src/main/resources/dataRel.csv";
        Path myPath = Paths.get(fileName);

        try (BufferedReader br = Files.newBufferedReader(myPath,
                StandardCharsets.UTF_8)) {

            HeaderColumnNameMappingStrategy<Rel> strategy
                    = new HeaderColumnNameMappingStrategy<>();
            strategy.setType(Rel.class);

            CsvToBean csvToBean = new CsvToBeanBuilder(br)
                    .withType(Rel.class)
                    .withMappingStrategy(strategy)
                    .withIgnoreLeadingWhiteSpace(true)
                    .build();

            relations = csvToBean.parse();
        }
        catch (Exception e){
            e.printStackTrace();
        }

        try {
            FileOutputStream fos = new FileOutputStream("src/main/resources/relationssbeans.xml");
            BufferedOutputStream bos = new BufferedOutputStream(fos);
            XMLEncoder xmlEncoder = new XMLEncoder(bos);
            xmlEncoder.writeObject(relations);
            xmlEncoder.close();
        }
        catch (Exception e){
            e.printStackTrace();
        }

        return new ArrayList<>(relations);
    }
}
