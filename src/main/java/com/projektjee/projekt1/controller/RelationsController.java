package com.projektjee.projekt1.controller;

import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.projektjee.projekt1.domain.Person;
import com.projektjee.projekt1.domain.Rel;
import com.projektjee.projekt1.service.AppInMemoryService;
import com.projektjee.projekt1.service.PersonInMemoryService;
import com.projektjee.projekt1.service.RelInMemoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;

@Controller
public class RelationsController {

    @Autowired
    PersonInMemoryService ppl;
    @Autowired
    AppInMemoryService apps;
    @Autowired
    RelInMemoryService rels;

    @GetMapping("/addrel")
    public String add(@RequestParam int pid, @RequestParam int aid, @RequestParam(required = false) String site){
        if (rels.findByIds(aid, pid) == null && apps.findById(aid) != null && ppl.findById(pid) != null){
            rels.add(new Rel(pid, aid));
        }
        if (site != null) {
            if (site.equals("app"))
                return "redirect:/app/" + aid;
            else if (site.equals("person"))
                return "redirect:/person/" + pid;
        }
        return "redirect:/";
    }

    @GetMapping("/delrel")
    public String delete(@RequestParam int pid, @RequestParam int aid, @RequestParam(required = false) String site) {
        rels.remove(rels.findByIds(aid, pid));
        if (site != null) {
            if (site.equals("app"))
                return "redirect:/app/" + aid;
            else if (site.equals("person"))
                return "redirect:/person/" + pid;
        }
        return "redirect:/";
    }

    @GetMapping("/relcsv")
    public void exportCSV(HttpServletResponse response) throws Exception {

        //set file name and content type
        String filename = "relations.csv";

        response.setContentType("text/csv");
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + filename + "\"");

        //create a csv writer
        StatefulBeanToCsv<Rel> writer = new StatefulBeanToCsvBuilder<Rel>(response.getWriter())
                .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
                .withSeparator(CSVWriter.DEFAULT_SEPARATOR)
                .withOrderedResults(false)
                .build();

        //write all users to csv file
        writer.write(rels.getAllRelations());

    }


}
