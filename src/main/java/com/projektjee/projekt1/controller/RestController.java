package com.projektjee.projekt1.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.projektjee.projekt1.domain.App;
import com.projektjee.projekt1.domain.Person;
import com.projektjee.projekt1.service.AppInMemoryService;
import com.projektjee.projekt1.service.PersonInMemoryService;
import com.projektjee.projekt1.service.RelInMemoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.core.io.ByteArrayResource;
@org.springframework.web.bind.annotation.RestController
public class RestController {
    @Autowired
    private PersonInMemoryService ppl;
    @Autowired
    private AppInMemoryService apps;
    @Autowired
    private RelInMemoryService rels;


    @GetMapping("/api/person/{id}")
    Person getPerson(@PathVariable Integer id) {
        return ppl.findById(id);
    }

    @GetMapping(value = "/api/app/{id}",  produces = MediaType.APPLICATION_JSON_VALUE)
    String getApp(@PathVariable Integer id) {
        App app = apps.findById(id);
        String s = "{\"" + app.getName() + "\":{\"Domain\":\"" + app.getDomain() + "\", \"People\":" + String.valueOf(rels.findPeopleByAppId(id).size()) + "}}";
        return s;
    }

}
