package com.projektjee.projekt1.controller;

import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import com.projektjee.projekt1.domain.App;
import com.projektjee.projekt1.domain.Person;
import com.projektjee.projekt1.service.PersonInMemoryService;
import com.projektjee.projekt1.service.RelInMemoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

@Controller
public class PersonController {
    @Autowired
    PersonInMemoryService ppl;
    @Autowired
    RelInMemoryService rels;


    @GetMapping("/addperson")
    public String add(final Model model) {
        model.addAttribute("person", new Person());
        return "person-form";
    }

    @PostMapping("/addperson")
    public String add2(@Valid Person person, Errors errors) {
        if(errors.hasErrors()){
            return "person-form";
        }
        ppl.add(person);
        return "redirect:/";
    }

    @GetMapping("/person/edit/{id}")
    public String edit(@PathVariable("id") int id, Model model){
        model.addAttribute("person", ppl.findById(id));
        model.addAttribute("id", id);
        return "person-edit";
    }

    @PostMapping("/person/edit/{id}")
    public String edit2(@Valid Person person, Errors errors, @PathVariable("id") int id) {
        if(errors.hasErrors()){
            return "person-edit";
        }
        ppl.edit(person);
        return "redirect:/";
    }

    @GetMapping("/person/del/{id}")
    public String delete(@PathVariable("id") int id) {
        ppl.remove(id);
        rels.removeByPersonId(id);
        return "redirect:/ppl";
    }

    @RequestMapping("/ppl")
    public String home(final Model model) {
        model.addAttribute("ppl", ppl.getAllPersons());
        return "ppl-list";
    }

    @GetMapping("/personcsv")
    public void exportCSV(HttpServletResponse response) throws Exception {

        //set file name and content type
        String filename = "users.csv";

        response.setContentType("text/csv");
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + filename + "\"");

        //create a csv writer
        StatefulBeanToCsv<Person> writer = new StatefulBeanToCsvBuilder<Person>(response.getWriter())
                .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
                .withSeparator(CSVWriter.DEFAULT_SEPARATOR)
                .withOrderedResults(false)
                .build();

        //write all users to csv file
        writer.write(ppl.getAllPersons());

    }

    @GetMapping("person/{id}")
    public String into(@PathVariable("id") int id, Model model) {
        List<App> apps = rels.findAppsByPersonId(id);
        model.addAttribute("apps", apps);
        model.addAttribute("pid", id);
        model.addAttribute("site", "person");
        model.addAttribute("aid", new String());
        return "person-info";
    }
}
