package com.projektjee.projekt1.controller;

import com.projektjee.projekt1.domain.App;
import com.projektjee.projekt1.service.AppInMemoryService;
import com.projektjee.projekt1.service.PersonInMemoryService;
import com.projektjee.projekt1.service.RelInMemoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class HomeController {

    @Autowired
    PersonInMemoryService ppl;
    @Autowired
    AppInMemoryService apps;
    @Autowired
    RelInMemoryService rel;


    @RequestMapping("/")
    public String home(final Model model, @RequestParam(required = false) String id) {
        int l = -1;
        List<App> appList = apps.getAllApps();
        try
        {
            l = Integer.parseInt(id);
            appList = rel.findAppsByPersonId(l);
        }
        catch (NumberFormatException e)
        { }
        model.addAttribute("apps", appList);
        model.addAttribute("id", new String());
        return "home";
    }

}
