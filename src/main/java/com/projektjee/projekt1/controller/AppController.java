package com.projektjee.projekt1.controller;

import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.projektjee.projekt1.domain.App;
import com.projektjee.projekt1.domain.Person;
import com.projektjee.projekt1.domain.Rel;
import com.projektjee.projekt1.service.AppInMemoryService;
import com.projektjee.projekt1.service.PersonInMemoryService;
import com.projektjee.projekt1.service.RelationsManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@Controller
public class AppController {
    @Autowired
    AppInMemoryService apps;
    @Autowired
    PersonInMemoryService ppl;
    @Autowired
    RelationsManager res;

    @GetMapping("/addapp")
    public String add(final Model model) {
        model.addAttribute("app", new App());
        return "app-form";
    }

    @PostMapping("/addapp")
    public String add2(@Valid App app, Errors errors) {
        if(errors.hasErrors()){
            return "app-form";
        }
        apps.add(app);
        return "redirect:/";
    }

    @GetMapping("/app/edit/{id}")
    public String edit(@PathVariable("id") int id, Model model){
        model.addAttribute("app", apps.findById(id));
        model.addAttribute("id", id);
        return "app-edit";
    }

    @PostMapping("/app/edit/{id}")
    public String edit2(@Valid App app, Errors errors, @PathVariable("id") int id) {
        if(errors.hasErrors()){
            return "app-edit";
        }
        apps.edit(app);
        return "redirect:/";
    }

    @GetMapping("/app/del/{id}")
    public String delete(@PathVariable("id") int id) {
        apps.remove(id);
        res.removeByAppId(id);
        return "redirect:/";
    }

    @GetMapping("app/{id}")
    public String into(@PathVariable("id") int id, Model model) {
        List<Person> people = res.findPeopleByAppId(id);
        Map<String, Integer> map = ppl.countCountries(people);
        model.addAttribute("ppl", people);
        model.addAttribute("aid", id);
        model.addAttribute("site", "app");
        model.addAttribute("map", map);
        model.addAttribute("pid", new String());
        return "app-info";
    }


    @GetMapping("/appcsv")
    public void exportCSV(HttpServletResponse response) throws Exception {

        //set file name and content type
        String filename = "apps.csv";

        response.setContentType("text/csv");
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + filename + "\"");

        //create a csv writer
        StatefulBeanToCsv<App> writer = new StatefulBeanToCsvBuilder<App>(response.getWriter())
                .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
                .withSeparator(CSVWriter.DEFAULT_SEPARATOR)
                .withOrderedResults(false)
                .build();

        //write all users to csv file
        writer.write(apps.getAllApps());

    }


}
