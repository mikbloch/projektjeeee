package com.projektjee.projekt1;


import javax.validation.*;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import static org.junit.Assert.*;

import com.projektjee.projekt1.domain.Person;
import com.projektjee.projekt1.service.AppInMemoryService;
import com.projektjee.projekt1.service.PersonInMemoryService;
import com.projektjee.projekt1.service.RelInMemoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.validation.ConstraintViolation;
import java.util.Set;

@SpringBootTest
class Projekt1pplicationTests {


	private Validator validator;

	@Autowired
	PersonInMemoryService ppl;
	@Autowired
	AppInMemoryService apps;
	@Autowired
	RelInMemoryService rel;


	@Before
	public void setUp() {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		validator = factory.getValidator();
	}

	@Test
	void loading() {
		assertEquals(200, ppl.getAllPersons().size());
		assertEquals(200, apps.getAllApps().size());
	}
}
